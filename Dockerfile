FROM registry.gitlab.com/codelibre/containers/ome-files-build-ubuntu-22.04-noxerces:latest
MAINTAINER rleigh@codelibre.net

RUN apt-get update \
  && DEBIAN_FRONTEND=noninteractive apt-get remove -y -qq --no-install-recommends \
    'libboost*-dev' \
  && DEBIAN_FRONTEND=noninteractive apt-get install libboost-dev -y -qq
